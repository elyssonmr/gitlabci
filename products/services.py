from bson import ObjectId


class InvalidProduct(Exception):
    def __init__(self, errors):
        self.errors = errors


class ProductService:
    def __init__(self, collection):
        self._collection = collection

    def _extract_product(self, product):
        if not product:
            return {}
        product["id"] = str(product["_id"])
        del product["_id"]
        return product

    async def get_products(self):
        mongo_products = await self._collection.find().to_list(None)
        products = []
        for product in mongo_products:
            products.append(self._extract_product(product))

        return products

    async def get_product(self, product_id):
        product_id = ObjectId(product_id)
        product = await self._collection.find_one({"_id": product_id})
        return self._extract_product(product)

    async def insert_product(self, product):
        errors = self._validate_product(product)
        if errors:
            raise InvalidProduct(errors)

        result = await self._collection.insert_one(product)
        return str(result.inserted_id)

    async def update_product(self, product):
        errors = self._validate_product(product)
        if errors:
            raise InvalidProduct(errors=errors)

        prod_id = ObjectId(product.pop("id"))
        result = await self._collection.update_one(
            {"_id": prod_id}, {"$set": product})
        return bool(result.modified_count)

    async def delete_product(self, prod_id):
        delete_filter = {"_id": ObjectId(prod_id)}
        result = await self._collection.delete_one(delete_filter)
        return bool(result.deleted_count)

    def _validate_product(self, product):
        errors = {}
        name = product.get("name", "")
        value = product.get("value", -1.0)
        description = product.get("description", "")

        if not name or not isinstance(name, str):
            errors["name"] = "Name is a required String"

        if not isinstance(value, float) or value < 0:
            errors["value"] = "Value should be a positive float"

        if not description or not isinstance(description, str):
            errors["description"] = "Description is a required String"

        return errors
