from tornado.web import Application

from motor.motor_asyncio import AsyncIOMotorClient

from products import config
from products.handlers import ProductHandler, VersionHandler


def config_db(loop):
    return AsyncIOMotorClient(
        config.MONGO_URL, io_loop=loop
    ).get_database("productsdb")


def create_app(loop):
    handlers = [
        (r"^/api/products(?:/(?P<product_id>[^/]+))?$", ProductHandler),
        (r"^/version$", VersionHandler)
    ]
    settings = {
        "db": config_db(loop),
        "debug": config.DEBUG
    }
    return Application(handlers, **settings)
