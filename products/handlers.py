import json

from tornado.web import RequestHandler, HTTPError

from products.services import ProductService, InvalidProduct

from products import config


class BaseHandler(RequestHandler):
    def write_error(self, status_code, **kwargs):
        exc = kwargs["exc_info"]
        self.set_status(status_code)
        self.finish({"error": str(exc), "status_code": status_code})

    @property
    def db(self):
        return self.settings["db"]


class VersionHandler(BaseHandler):
    def get(self):
        self.write({"name": "Products API", "version": config.VERSION})


class ProductHandler(BaseHandler):
    async def get(self, product_id=None):

        service = ProductService(self.db["products"])
        if product_id:
            data = await service.get_product(product_id)
        else:
            data = await service.get_products()
        self.write({"data": data})

    async def post(self, product_id):
        if product_id:
            raise HTTPError(400)
        service = ProductService(self.db["products"])
        product = json.loads(self.request.body)
        try:
            inserted_id = await service.insert_product(product)
        except InvalidProduct as ex:
            self.set_status(400)
            self.write(ex.errors)
            return
        self.set_status(201)
        self.write({"insertedId": inserted_id})

    async def put(self, product_id):
        service = ProductService(self.db["products"])
        product = json.loads(self.request.body)
        product["id"] = product_id
        try:
            await service.update_product(product)
        except InvalidProduct as ex:
            self.set_status(400)
            self.write(ex.errors)
            return

    async def delete(self, product_id):
        service = ProductService(self.db["products"])
        await service.delete_product(product_id)
