import decouple

VERSION = "1.0.0"

DEBUG = decouple.config("DEBUG", default="0", cast=bool)
MONGO_URL = decouple.config("MONGO_URL", default='mongodb://localhost:27017')
