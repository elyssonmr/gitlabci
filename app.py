import asyncio
import logging

from products.application import create_app

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    app = create_app(loop)
    app.listen(8888)
    logger.info("Products API started at port 8888.")
    loop.run_forever()
