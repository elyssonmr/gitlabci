check:
	flake8 --config=ci-config/flake8

test:
	pytest -vv -s --cov=. --cov-report html:coverage --cov-report term-missing .

docker:
	docker-compose up -d --build

bdd:
	python -m robot -d ./atdd/results -L debug -v MONGO_HOST:localhost ./atdd/testcases
