import json
from unittest.mock import Mock, patch

from tornado.testing import AsyncHTTPTestCase
from tornado.web import Application

from products.handlers import ProductHandler
from products.services import InvalidProduct

from tests import setup_future


class ProductHandlerTestCase(AsyncHTTPTestCase):
    def setUp(self):
        self.collection = Mock()
        super().setUp()

    def get_app(self):
        return Application([
            (r"^/api/products(?:/(?P<product_id>[^/]+))?$", ProductHandler)
        ], db={"products": self.collection})

    @patch("products.handlers.ProductService")
    def test_get_products(self, _prod_service):
        products = [{"prod1": "1"}, {"prod2": "2"}]
        service = Mock()
        service.get_products.return_value = setup_future(products)
        _prod_service.return_value = service
        expected = {
            "data": products
        }

        response = self.fetch("/api/products")

        body = json.loads(response.body)
        self.assertEqual(response.code, 200)
        self.assertEqual(body, expected)
        _prod_service.assert_called_once_with(self.collection)
        service.get_products.assert_called_once_with()

    @patch("products.handlers.ProductService")
    def test_get_product(self, _prod_service):
        product = {"prod1": "1"}
        service = Mock()
        service.get_product.return_value = setup_future(product)
        _prod_service.return_value = service
        product_id = "123456abc"
        expected = {
            "data": product
        }

        response = self.fetch(f"/api/products/{product_id}")

        body = json.loads(response.body)
        self.assertEqual(response.code, 200)
        self.assertEqual(body, expected)
        _prod_service.assert_called_once_with(self.collection)
        service.get_product.assert_called_once_with(product_id)

    @patch("products.handlers.ProductService")
    def test_post_product(self, _prod_service):
        product_id = "123456abc"
        product = {"newProd": "1"}
        service = Mock()
        service.insert_product.return_value = setup_future(product_id)
        _prod_service.return_value = service

        expected = {
            "insertedId": product_id
        }

        response = self.fetch("/api/products", method="POST",
                              body=json.dumps(product))

        body = json.loads(response.body)
        self.assertEqual(response.code, 201)
        self.assertEqual(body, expected)
        _prod_service.assert_called_once_with(self.collection)
        service.insert_product.assert_called_once_with(product)

    @patch("products.handlers.ProductService")
    def test_put_product(self, _prod_service):
        product_id = "123456abc"
        product = {"newProd": "1", "id": product_id}
        service = Mock()
        service.update_product.return_value = setup_future(product_id)
        _prod_service.return_value = service

        response = self.fetch(f"/api/products/{product_id}", method="PUT",
                              body=json.dumps(product))

        self.assertEqual(response.code, 200)
        _prod_service.assert_called_once_with(self.collection)
        service.update_product.assert_called_once_with(product)

    @patch("products.handlers.ProductService")
    def test_delete_product(self, _prod_service):
        product_id = "123456abc"
        service = Mock()
        service.delete_product.return_value = setup_future(product_id)
        _prod_service.return_value = service

        response = self.fetch(f"/api/products/{product_id}", method="DELETE")

        self.assertEqual(response.code, 200)
        _prod_service.assert_called_once_with(self.collection)
        service.delete_product.assert_called_once_with(product_id)

    @patch("products.handlers.ProductService")
    def test_insert_invalid_product(self, _prod_service):
        product = {
            "value": 9.0,
            "description": "Teste Invalid Product"
        }
        service = Mock()
        error = {"name": "Name is a required String"}
        service.insert_product.side_effect = InvalidProduct(error)
        _prod_service.return_value = service
        expected = error

        response = self.fetch("/api/products", method="POST",
                              body=json.dumps(product))

        body = json.loads(response.body)
        self.assertEqual(response.code, 400)
        self.assertEqual(body, expected)
        _prod_service.assert_called_once_with(self.collection)
        service.insert_product.assert_called_once_with(product)

    @patch("products.handlers.ProductService")
    def test_update_invalid_product(self, _prod_service):
        prod_id = "123456abc"
        product = {
            "value": 9.0,
            "description": "Teste Invalid Product"
        }
        service = Mock()
        error = {"name": "Name is a required String"}
        service.update_product.side_effect = InvalidProduct(error)
        _prod_service.return_value = service
        expected = error
        expected_call = product.copy()
        expected_call.update({"id": prod_id})

        response = self.fetch(f"/api/products/{prod_id}", method="PUT",
                              body=json.dumps(product))

        body = json.loads(response.body)
        self.assertEqual(response.code, 400)
        self.assertEqual(body, expected)
        _prod_service.assert_called_once_with(self.collection)
        service.update_product.assert_called_once_with(expected_call)
