from unittest.mock import Mock

from tornado.testing import AsyncTestCase, gen_test
from bson import ObjectId

from products.services import ProductService

from tests import setup_future


class ProductControllerTestCase(AsyncTestCase):
    def setUp(self):
        super().setUp()
        self.collection = Mock()
        self.service = ProductService(self.collection)

    @gen_test
    async def test_get_products(self):
        id1 = ObjectId()
        id2 = ObjectId()
        mongo_data = [
            {"_id": id1, "name": "product1", "value": 10.58,
             "description": "desc1"},
            {"_id": id2, "name": "product2", "value": 9,
             "description": "desc2"}
        ]
        get_mock = Mock()
        get_mock.to_list.return_value = setup_future(mongo_data)
        self.collection.find.return_value = get_mock
        expected = [
            {"id": str(id1), "name": "product1", "value": 10.58,
             "description": "desc1"},
            {"id": str(id2), "name": "product2", "value": 9,
             "description": "desc2"},
        ]

        resp = await self.service.get_products()

        self.assertEqual(resp, expected)
        self.collection.find.assert_called_once_with()
        get_mock.to_list.assert_called_once_with(None)

    @gen_test
    async def test_get_product(self):
        product_id = ObjectId()
        mongo_data = {"_id": product_id, "name": "product1", "value": 10.58,
                      "description": "desc1"}
        self.collection.find_one.return_value = setup_future(mongo_data)
        expected = {"id": str(product_id), "name": "product1", "value": 10.58,
                    "description": "desc1"}

        resp = await self.service.get_product(product_id)

        self.assertEqual(resp, expected)
        self.collection.find_one.assert_called_once_with({"_id": product_id})

    @gen_test
    async def test_create_product(self):
        product = {"name": "newProduct",
                   "value": 10.50, "description": "New Prod"}
        result = Mock()
        result.inserted_id = ObjectId()
        self.collection.insert_one.return_value = setup_future(result)
        expected = str(result.inserted_id)

        resp = await self.service.insert_product(product)

        self.assertEqual(resp, expected)
        self.collection.insert_one.assert_called_once_with(product)

    @gen_test
    async def test_update_product(self):
        prod_id = ObjectId()
        product = {"id": str(prod_id), "name": "prodUpdate", "value": 5.90,
                   "description": "DescUpdate"}
        expected_update = product.copy()
        del expected_update["id"]
        result = Mock(modified_count=1)
        self.collection.update_one.return_value = setup_future(result)

        resp = await self.service.update_product(product)

        self.assertTrue(resp)
        self.collection.update_one.assert_called_once_with(
            {"_id": prod_id}, {"$set": expected_update})

    @gen_test
    async def test_delete_product(self):
        prod_id = ObjectId()
        result = Mock(deleted_count=1)
        self.collection.delete_one.return_value = setup_future(result)

        resp = await self.service.delete_product(str(prod_id))

        self.assertTrue(resp)
        self.collection.delete_one.assert_called_once_with({"_id": prod_id})


class ValidateProductTestCase(AsyncTestCase):
    def setUp(self):
        super().setUp()
        self.collection = Mock()
        self.service = ProductService(self.collection)

    def test_validate_product(self):
        product = {
            "name": "product2", "value": 9.0,
            "description": "desc2"
        }
        errors = self.service._validate_product(product)

        self.assertDictEqual(errors, {})

    def test_validate_invalid_product_name(self):
        product = {
            "name": "",
            "value": 9.25,
            "description": "Product without name"
        }

        errors = self.service._validate_product(product)

        self.assertDictEqual(errors, {"name": "Name is a required String"})

    def test_valiedate_invalid_product_value(self):
        product = {
            "name": "Priceless product",
            "value": "",
            "description": "Not for free"
        }

        errors = self.service._validate_product(product)

        self.assertDictEqual(errors,
            {"value": "Value should be a positive float"})

    def test_validate_invalid_product_description(self):
        product = {
            "name": "My product",
            "value": 99999.99
        }

        errors = self.service._validate_product(product)

        self.assertDictEqual(errors,
            {"description": "Description is a required String"})
