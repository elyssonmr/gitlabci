*** Settings ***
Library         RequestsLibrary
Library         ../libraries/MongoLibraryCustom.py

*** Keywords ***
Inicializar teste
    Conectar ao Mongo
    Limpar registros do mongoDB    products

Limpar mongo
    Conectar ao Mongo
    Limpar registros do mongoDB    products

Encerrar teste
    Fechar conexão com o Banco de dados

For cadastrado o produto com nome "${NOME}", valor "${VALOR}" e descrição "${DESCRICAO}"
    &{NOTIFICAÇÃO} =    Create Dictionary    tipo=${VAR_TIPO}   titulo=${VAR_TÍTULO}  data=${VAR_DATA}  subtitulo=${SUBTITULO}  mensagem=${VAR_MENSAGEM}  categoria=${VAR_CATEGORIA}  informacoes=${VAR_INFORMAÇÃO}  tempo=${VAR_TEMPO}
    Set Test Variable   ${NOTIFICAÇÃO}
    Set Test Variable   ${NOME}
    Gerar mensagem de requisição - validação campo subtitulo    ${NOTIFICAÇÃO}
    Realizar requisição do endpoint (método POST)   ${ENDPOINT}/${NOME}   ${MENSAGEM_CONSULTA}

Cadastrar a notificação com sucesso
    Confere dados do produto gravados no CACHE    ${NOME}    ${NOTIFICAÇÃO}
