*** Settings ***
Library         RequestsLibrary
Library         ../libraries/MongoLibraryCustom.py

*** Variables ***
${PORT}          8000
${HOST}          http://localhost:${PORT}
${MONGO_HOST}    localhost
${MONGO_PORT}    27017
${MONGO_DB}      productsdb
${MONGO_COLLECTION}    products


*** Keywords ***
Conectar ao Serviço Products API
    Set Global Variable    ${HOST}
    Create Session          prod_API    ${HOST}
    Log                     Sessão criada no Host ${HOST}

Conectar ao Mongo
    Connect To Mongodb      ${MONGO_HOST}   ${MONGO_PORT}
    Set Global Variable     ${MONGO_DB}
    Log                     Sessão criada no Mongo Host ${MONGO_HOST}

Fechar conexão com o Banco de dados
    Disconnect From Mongodb

Limpar registros do mongoDB
    [Arguments]      ${MONGO_DB}
    ${RETORNO}       Remove Mongodb Records    ${MONGO_DB}    ${MONGO_COLLECTION}    {}

Realizar requisição do endpoint (método GET)
    [Arguments]                     ${ENDPOINT}
    ${RESPOSTA}=                    GET Request    prod_API  ${ENDPOINT}
    Log                             Resposta Retornada: ${RESPOSTA.status_code} ${RESPOSTA.reason} - ${RESPOSTA.text}
    Set Test Variable               ${RESPOSTA}

Realizar requisição do endpoint (método POST)
    [Arguments]                     ${ENDPOINT}     ${MENSAGEM}
    ${RESPOSTA}=                    POST Request    prod_API  ${ENDPOINT}     ${MENSAGEM}
    Log                             Resposta Retornada: ${RESPOSTA.status_code} ${RESPOSTA.reason} - ${RESPOSTA.text}
    Set Test Variable               ${RESPOSTA}

Que estou conectado na Products API
    Conectar ao Serviço Products API

Deve ser retornado o código de status "${STATUS}"
    Conferir status code    ${STATUS}

Conferir status code
    [Arguments]                     ${CODIGO_ESPERADO}
    Log                             Status Code esperado: ${CODIGO_ESPERADO} -- Status Code retornado: ${RESPOSTA.status_code} ${RESPOSTA.reason}
    Should Be Equal As Strings      ${RESPOSTA.status_code}  ${CODIGO_ESPERADO}
